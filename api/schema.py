import graphene


class Query(graphene.ObjectType):
    """Project level Query class
    this class will inherit multiple queries from the apps
    """

    car = graphene.String()

    # Test Query
    def resolve_car(self, info):
        return "Mercedes Benz | Model:23qwer | Color: Black"


class Mutation(graphene.ObjectType):
    """Project level Mutation Class
    this class will inherit multiple mutations from the apps
    """

    pass


schema = graphene.Schema(query=Query)
